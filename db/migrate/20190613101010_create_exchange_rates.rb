class CreateExchangeRates < ActiveRecord::Migration[5.2]
  def change
    create_table :exchange_rates do |t|
      t.string :from, limit: 5
      t.string :to, limit: 5
      t.string :buy, limit: 10
      t.string :sell, limit: 10
      t.string :timestamp, limit: 20

      t.timestamps
    end
  end
end

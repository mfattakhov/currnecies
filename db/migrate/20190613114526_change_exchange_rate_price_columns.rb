class ChangeExchangeRatePriceColumns < ActiveRecord::Migration[5.2]
  def change
    change_column :exchange_rates, :buy, 'decimal USING CAST(buy AS decimal)'
    change_column :exchange_rates, :sell, 'decimal USING CAST(sell AS decimal)'
  end
end

# Service class that encapsulates the logic
# of retrieving currency rates from a single API endpoint.
# CurrenciesService defined as Singleton to share the same object across threads.
class CurrenciesService
    include Singleton
    
    # Basic error to handle CurrenciesService errors/exceptions.
    class Error < StandardError
    end
    
    # Rates pulling API endpoint.
    RATES_API_URL = 'https://www.tinkoff.ru/api/v1/currency_rates/'
    # HTTP codes to think that pull response succeed.
    RATES_API_SUCCESS_CODES = [200]
    # 
    RATES_PAIRS_TO_RETRIVE = [['USD', 'RUB'], ['EUR', 'RUB']]
    # Internal logger label to mark current class logs.
    LOGGER_LABEL = 'CurrenciesService: '

    def initialize
        @http_client = HTTPClient.new
        @logger = Rails.logger
    end

    # Gets current exchange rates and saves them in db.
    def retrieve_current_rates
        @logger.info("#{LOGGER_LABEL}rates retrieve started.")
        response = _get_current_rates
        if response
            timestamp = response['payload']['lastUpdate']['milliseconds']
            @logger.info("#{LOGGER_LABEL}rates were retrieved for timestamp: #{timestamp}.")
            rates = response['payload']['rates']
            rates.each {|rate| ExchangeRate.create(from: rate['fromCurrency']['name'],
                                                   to: rate['toCurrency']['name'],
                                                   buy: rate['buy'].to_s,
                                                   sell: rate['sell'].to_s,
                                                   timestamp: timestamp) if _rate_should_be_retrieved? rate}
            @logger.info("#{LOGGER_LABEL}rates retrieve succeed for timestamp: #{timestamp}")
        end
        nil
    end

    private

    # Method defines rules for the rate dict if it should be saved.
    def _rate_should_be_retrieved?(rate)
        rate['category'] == 'DepositPayments' && RATES_PAIRS_TO_RETRIVE.include?([rate['fromCurrency']['name'], rate['toCurrency']['name']])
    end

    # Returns current rates data from RATES_API_URL.
    def _get_current_rates
        begin
            response = @http_client.get(RATES_API_URL)
            raise CurrenciesService::Error.new("Service returned: #{response.code}") unless RATES_API_SUCCESS_CODES.include? response.code
            response.parsed_response
        rescue StandardError => e
            @logger.error(e.inspect)
        end
    end
end

# HTTP client class to make outgoing requests.
# Class could be extended for other request types,
# to use other HTTP libraries, to add metrics, etc.
class HTTPClient
    def get(url)
        HTTParty.get(url)
    end
end

class Api::V1::ExchangeRatesController < ApplicationController
  def index
    date = params['date'].to_date rescue Date.today
    @rates = ExchangeRate.find_by_date(date)
    @datasets = {}
    @rates.each do |rate|
      @datasets["#{rate.to_s}:buy"] = [] unless @datasets.key? "#{rate.to_s}:buy"
      @datasets["#{rate.to_s}:sell"] = [] unless @datasets.key? "#{rate.to_s}:sell"
      @datasets["#{rate.to_s}:buy"] << {y: rate.buy, x: rate.timestamp}
      @datasets["#{rate.to_s}:sell"] << {y: rate.sell, x: rate.timestamp}
    end
    render json: @datasets
  end
end

class ExchangeRate < ApplicationRecord

    def to_s
        "#{from}:#{to}"
    end

    # Get all the rates by date.
    def self.find_by_date(date)
        start_timestamp = date.beginning_of_day.to_i.to_s
        end_timestamp = date.end_of_day.to_i.to_s
        ExchangeRate.where({timestamp: (start_timestamp..end_timestamp)})
    end

    # Get avg buy, sell rates by date for specific pair of currencies.
    def self.average_rates_by_day(date, from, to)
        rates = find_by_date(date).where({from: from, to: to})
        {sell: rates.average(:sell), buy: rates.average(:buy)}
    end

end

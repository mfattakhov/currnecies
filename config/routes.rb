Rails.application.routes.draw do
  root to: 'welcome#index'
  namespace :api do
    namespace :v1 do
      resources :exchange_rates, only: [:index]
    end
  end
end

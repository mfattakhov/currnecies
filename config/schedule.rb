ENV.each { |k, v| env(k, v) }

set :output, "/code/log/cron_log.log"
set :environment, ENV.fetch("RAILS_ENV") {'development'}

every 1.hours do
  runner "CurrenciesService.instance.retrieve_current_rates"
end

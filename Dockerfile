FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client cron
RUN mkdir /code
WORKDIR /code
COPY Gemfile /code/Gemfile
# COPY Gemfile.lock /code/Gemfile.lock
RUN bundle install
COPY . /code

RUN whenever --update-crontab
RUN service cron start

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
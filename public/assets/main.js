var utils = {
    getRandomRBG: () => {
        var x = Math.floor(Math.random() * 256);
        var y = Math.floor(Math.random() * 256);
        var z = Math.floor(Math.random() * 256);
        return "rgb(" + x + "," + y + "," + z + ")";
    },
    getDatasets: (successHandler) => {
        axios.get('/api/v1/exchange_rates?date=${Date.}')
        .then((response) => {
            successHandler(response.data);
        })
        .catch((error) => {
            console.log(error);
        });
    },
    createChart: () => {
        let ctx = document.getElementById('exchangeRates').getContext('2d');
        return new Chart(ctx, {
            type: 'line'
        });
    }
};

(() => {
    let chart = utils.createChart();
    utils.getDatasets((data) => {
        for (let name in data) {
            chart.data.datasets.push({
                label: name,
                borderColor: utils.getRandomRBG(),
                data: data[name],
                fill: false
            });
        }
        chart.update();
    })
})();
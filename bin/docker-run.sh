#!/bin/bash
whenever --update-crontab
service cron start
rm -f tmp/pids/server.pid && bundle exec rails s -p 3000 -b '0.0.0.0'